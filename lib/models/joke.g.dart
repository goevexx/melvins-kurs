// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Joke _$$_JokeFromJson(Map<String, dynamic> json) => _$_Joke(
      json['id'] as int,
      json['joke'] as String,
      (json['categories'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$$_JokeToJson(_$_Joke instance) => <String, dynamic>{
      'id': instance.id,
      'joke': instance.joke,
      'categories': instance.categories,
    };
