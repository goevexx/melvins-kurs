import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_pages_and_forms/models/joke.dart';

part 'joke_request.freezed.dart';
part 'joke_request.g.dart';

@freezed
class JokeRequest with _$JokeRequest {
  const factory JokeRequest({required String type, required Joke value}) =
      _JokeRequest;
  factory JokeRequest.fromJson(Map<String, dynamic> map) =>
      _$JokeRequestFromJson(map);
}
