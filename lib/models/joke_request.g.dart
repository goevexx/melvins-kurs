// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_JokeRequest _$$_JokeRequestFromJson(Map<String, dynamic> json) =>
    _$_JokeRequest(
      type: json['type'] as String,
      value: Joke.fromJson(json['value'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_JokeRequestToJson(_$_JokeRequest instance) =>
    <String, dynamic>{
      'type': instance.type,
      'value': instance.value,
    };
