import 'package:freezed_annotation/freezed_annotation.dart';

part 'recipes.freezed.dart';

@freezed
class Recipe with _$Recipe {
  const factory Recipe({
    required String name,
    required String zubereitung,
    required List<String> zutaten,
  }) = _Recipe;
}
