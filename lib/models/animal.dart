import 'package:freezed_annotation/freezed_annotation.dart';

part 'animal.freezed.dart';
part 'animal.g.dart';

@freezed
class Animal with _$Animal {
  const factory Animal({required String name}) = _Animal;
  const factory Animal.dog({required String name}) = _DogAnimal;
  const factory Animal.cat({required String name}) = _CatAnimal;

  factory Animal.fromJson(Map<String, dynamic> json) => _$AnimalFromJson(json);
}

void handleAnimal(Animal animal) {
  animal.copyWith.call(name: '');
  animal.toJson();
  Animal.dog(name: 'name');
}

class Cat {
  final String _name;
  Cat(this._name) {
    print('Cat spawned!');
  }
  call(String name) {
    print(name);
  }
}

extension ThisExtensionX on Cat {
  static const String gender = 'BI';
  sound() {
    print('Miiaaaw');
  }
}
