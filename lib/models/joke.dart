import 'package:freezed_annotation/freezed_annotation.dart';

part 'joke.freezed.dart';
part 'joke.g.dart';

@freezed
class Joke with _$Joke {
  const factory Joke(int id, String joke, List<String> categories) = _Joke;
  factory Joke.fromJson(Map<String, dynamic> map) => _$JokeFromJson(map);
}
