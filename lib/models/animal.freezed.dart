// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'animal.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Animal _$AnimalFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String?) {
    case 'default':
      return _Animal.fromJson(json);
    case 'dog':
      return _DogAnimal.fromJson(json);
    case 'cat':
      return _CatAnimal.fromJson(json);

    default:
      throw CheckedFromJsonException(json, 'runtimeType', 'Animal',
          'Invalid union type "${json['runtimeType']}"!');
  }
}

/// @nodoc
class _$AnimalTearOff {
  const _$AnimalTearOff();

  _Animal call({required String name}) {
    return _Animal(
      name: name,
    );
  }

  _DogAnimal dog({required String name}) {
    return _DogAnimal(
      name: name,
    );
  }

  _CatAnimal cat({required String name}) {
    return _CatAnimal(
      name: name,
    );
  }

  Animal fromJson(Map<String, Object> json) {
    return Animal.fromJson(json);
  }
}

/// @nodoc
const $Animal = _$AnimalTearOff();

/// @nodoc
mixin _$Animal {
  String get name => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String name) $default, {
    required TResult Function(String name) dog,
    required TResult Function(String name) cat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Animal value) $default, {
    required TResult Function(_DogAnimal value) dog,
    required TResult Function(_CatAnimal value) cat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AnimalCopyWith<Animal> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnimalCopyWith<$Res> {
  factory $AnimalCopyWith(Animal value, $Res Function(Animal) then) =
      _$AnimalCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class _$AnimalCopyWithImpl<$Res> implements $AnimalCopyWith<$Res> {
  _$AnimalCopyWithImpl(this._value, this._then);

  final Animal _value;
  // ignore: unused_field
  final $Res Function(Animal) _then;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$AnimalCopyWith<$Res> implements $AnimalCopyWith<$Res> {
  factory _$AnimalCopyWith(_Animal value, $Res Function(_Animal) then) =
      __$AnimalCopyWithImpl<$Res>;
  @override
  $Res call({String name});
}

/// @nodoc
class __$AnimalCopyWithImpl<$Res> extends _$AnimalCopyWithImpl<$Res>
    implements _$AnimalCopyWith<$Res> {
  __$AnimalCopyWithImpl(_Animal _value, $Res Function(_Animal) _then)
      : super(_value, (v) => _then(v as _Animal));

  @override
  _Animal get _value => super._value as _Animal;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(_Animal(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Animal implements _Animal {
  const _$_Animal({required this.name});

  factory _$_Animal.fromJson(Map<String, dynamic> json) =>
      _$$_AnimalFromJson(json);

  @override
  final String name;

  @override
  String toString() {
    return 'Animal(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Animal &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$AnimalCopyWith<_Animal> get copyWith =>
      __$AnimalCopyWithImpl<_Animal>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String name) $default, {
    required TResult Function(String name) dog,
    required TResult Function(String name) cat,
  }) {
    return $default(name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
  }) {
    return $default?.call(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Animal value) $default, {
    required TResult Function(_DogAnimal value) dog,
    required TResult Function(_CatAnimal value) cat,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
  }) {
    return $default?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_AnimalToJson(this)..['runtimeType'] = 'default';
  }
}

abstract class _Animal implements Animal {
  const factory _Animal({required String name}) = _$_Animal;

  factory _Animal.fromJson(Map<String, dynamic> json) = _$_Animal.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$AnimalCopyWith<_Animal> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$DogAnimalCopyWith<$Res> implements $AnimalCopyWith<$Res> {
  factory _$DogAnimalCopyWith(
          _DogAnimal value, $Res Function(_DogAnimal) then) =
      __$DogAnimalCopyWithImpl<$Res>;
  @override
  $Res call({String name});
}

/// @nodoc
class __$DogAnimalCopyWithImpl<$Res> extends _$AnimalCopyWithImpl<$Res>
    implements _$DogAnimalCopyWith<$Res> {
  __$DogAnimalCopyWithImpl(_DogAnimal _value, $Res Function(_DogAnimal) _then)
      : super(_value, (v) => _then(v as _DogAnimal));

  @override
  _DogAnimal get _value => super._value as _DogAnimal;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(_DogAnimal(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_DogAnimal implements _DogAnimal {
  const _$_DogAnimal({required this.name});

  factory _$_DogAnimal.fromJson(Map<String, dynamic> json) =>
      _$$_DogAnimalFromJson(json);

  @override
  final String name;

  @override
  String toString() {
    return 'Animal.dog(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _DogAnimal &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$DogAnimalCopyWith<_DogAnimal> get copyWith =>
      __$DogAnimalCopyWithImpl<_DogAnimal>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String name) $default, {
    required TResult Function(String name) dog,
    required TResult Function(String name) cat,
  }) {
    return dog(name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
  }) {
    return dog?.call(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
    required TResult orElse(),
  }) {
    if (dog != null) {
      return dog(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Animal value) $default, {
    required TResult Function(_DogAnimal value) dog,
    required TResult Function(_CatAnimal value) cat,
  }) {
    return dog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
  }) {
    return dog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
    required TResult orElse(),
  }) {
    if (dog != null) {
      return dog(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_DogAnimalToJson(this)..['runtimeType'] = 'dog';
  }
}

abstract class _DogAnimal implements Animal {
  const factory _DogAnimal({required String name}) = _$_DogAnimal;

  factory _DogAnimal.fromJson(Map<String, dynamic> json) =
      _$_DogAnimal.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$DogAnimalCopyWith<_DogAnimal> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$CatAnimalCopyWith<$Res> implements $AnimalCopyWith<$Res> {
  factory _$CatAnimalCopyWith(
          _CatAnimal value, $Res Function(_CatAnimal) then) =
      __$CatAnimalCopyWithImpl<$Res>;
  @override
  $Res call({String name});
}

/// @nodoc
class __$CatAnimalCopyWithImpl<$Res> extends _$AnimalCopyWithImpl<$Res>
    implements _$CatAnimalCopyWith<$Res> {
  __$CatAnimalCopyWithImpl(_CatAnimal _value, $Res Function(_CatAnimal) _then)
      : super(_value, (v) => _then(v as _CatAnimal));

  @override
  _CatAnimal get _value => super._value as _CatAnimal;

  @override
  $Res call({
    Object? name = freezed,
  }) {
    return _then(_CatAnimal(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CatAnimal implements _CatAnimal {
  const _$_CatAnimal({required this.name});

  factory _$_CatAnimal.fromJson(Map<String, dynamic> json) =>
      _$$_CatAnimalFromJson(json);

  @override
  final String name;

  @override
  String toString() {
    return 'Animal.cat(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CatAnimal &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @JsonKey(ignore: true)
  @override
  _$CatAnimalCopyWith<_CatAnimal> get copyWith =>
      __$CatAnimalCopyWithImpl<_CatAnimal>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(String name) $default, {
    required TResult Function(String name) dog,
    required TResult Function(String name) cat,
  }) {
    return cat(name);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
  }) {
    return cat?.call(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(String name)? $default, {
    TResult Function(String name)? dog,
    TResult Function(String name)? cat,
    required TResult orElse(),
  }) {
    if (cat != null) {
      return cat(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Animal value) $default, {
    required TResult Function(_DogAnimal value) dog,
    required TResult Function(_CatAnimal value) cat,
  }) {
    return cat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
  }) {
    return cat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Animal value)? $default, {
    TResult Function(_DogAnimal value)? dog,
    TResult Function(_CatAnimal value)? cat,
    required TResult orElse(),
  }) {
    if (cat != null) {
      return cat(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_CatAnimalToJson(this)..['runtimeType'] = 'cat';
  }
}

abstract class _CatAnimal implements Animal {
  const factory _CatAnimal({required String name}) = _$_CatAnimal;

  factory _CatAnimal.fromJson(Map<String, dynamic> json) =
      _$_CatAnimal.fromJson;

  @override
  String get name => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$CatAnimalCopyWith<_CatAnimal> get copyWith =>
      throw _privateConstructorUsedError;
}
