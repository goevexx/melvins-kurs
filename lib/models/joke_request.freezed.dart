// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'joke_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

JokeRequest _$JokeRequestFromJson(Map<String, dynamic> json) {
  return _JokeRequest.fromJson(json);
}

/// @nodoc
class _$JokeRequestTearOff {
  const _$JokeRequestTearOff();

  _JokeRequest call({required String type, required Joke value}) {
    return _JokeRequest(
      type: type,
      value: value,
    );
  }

  JokeRequest fromJson(Map<String, Object> json) {
    return JokeRequest.fromJson(json);
  }
}

/// @nodoc
const $JokeRequest = _$JokeRequestTearOff();

/// @nodoc
mixin _$JokeRequest {
  String get type => throw _privateConstructorUsedError;
  Joke get value => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $JokeRequestCopyWith<JokeRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $JokeRequestCopyWith<$Res> {
  factory $JokeRequestCopyWith(
          JokeRequest value, $Res Function(JokeRequest) then) =
      _$JokeRequestCopyWithImpl<$Res>;
  $Res call({String type, Joke value});

  $JokeCopyWith<$Res> get value;
}

/// @nodoc
class _$JokeRequestCopyWithImpl<$Res> implements $JokeRequestCopyWith<$Res> {
  _$JokeRequestCopyWithImpl(this._value, this._then);

  final JokeRequest _value;
  // ignore: unused_field
  final $Res Function(JokeRequest) _then;

  @override
  $Res call({
    Object? type = freezed,
    Object? value = freezed,
  }) {
    return _then(_value.copyWith(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Joke,
    ));
  }

  @override
  $JokeCopyWith<$Res> get value {
    return $JokeCopyWith<$Res>(_value.value, (value) {
      return _then(_value.copyWith(value: value));
    });
  }
}

/// @nodoc
abstract class _$JokeRequestCopyWith<$Res>
    implements $JokeRequestCopyWith<$Res> {
  factory _$JokeRequestCopyWith(
          _JokeRequest value, $Res Function(_JokeRequest) then) =
      __$JokeRequestCopyWithImpl<$Res>;
  @override
  $Res call({String type, Joke value});

  @override
  $JokeCopyWith<$Res> get value;
}

/// @nodoc
class __$JokeRequestCopyWithImpl<$Res> extends _$JokeRequestCopyWithImpl<$Res>
    implements _$JokeRequestCopyWith<$Res> {
  __$JokeRequestCopyWithImpl(
      _JokeRequest _value, $Res Function(_JokeRequest) _then)
      : super(_value, (v) => _then(v as _JokeRequest));

  @override
  _JokeRequest get _value => super._value as _JokeRequest;

  @override
  $Res call({
    Object? type = freezed,
    Object? value = freezed,
  }) {
    return _then(_JokeRequest(
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as Joke,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_JokeRequest implements _JokeRequest {
  const _$_JokeRequest({required this.type, required this.value});

  factory _$_JokeRequest.fromJson(Map<String, dynamic> json) =>
      _$$_JokeRequestFromJson(json);

  @override
  final String type;
  @override
  final Joke value;

  @override
  String toString() {
    return 'JokeRequest(type: $type, value: $value)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _JokeRequest &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.value, value) ||
                const DeepCollectionEquality().equals(other.value, value)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(value);

  @JsonKey(ignore: true)
  @override
  _$JokeRequestCopyWith<_JokeRequest> get copyWith =>
      __$JokeRequestCopyWithImpl<_JokeRequest>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_JokeRequestToJson(this);
  }
}

abstract class _JokeRequest implements JokeRequest {
  const factory _JokeRequest({required String type, required Joke value}) =
      _$_JokeRequest;

  factory _JokeRequest.fromJson(Map<String, dynamic> json) =
      _$_JokeRequest.fromJson;

  @override
  String get type => throw _privateConstructorUsedError;
  @override
  Joke get value => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$JokeRequestCopyWith<_JokeRequest> get copyWith =>
      throw _privateConstructorUsedError;
}
