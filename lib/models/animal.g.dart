// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'animal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Animal _$$_AnimalFromJson(Map<String, dynamic> json) => _$_Animal(
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_AnimalToJson(_$_Animal instance) => <String, dynamic>{
      'name': instance.name,
    };

_$_DogAnimal _$$_DogAnimalFromJson(Map<String, dynamic> json) => _$_DogAnimal(
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_DogAnimalToJson(_$_DogAnimal instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

_$_CatAnimal _$$_CatAnimalFromJson(Map<String, dynamic> json) => _$_CatAnimal(
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_CatAnimalToJson(_$_CatAnimal instance) =>
    <String, dynamic>{
      'name': instance.name,
    };
