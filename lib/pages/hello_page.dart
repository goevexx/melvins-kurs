import 'package:flutter/material.dart';

class HelloPage extends StatefulWidget {
  HelloPage({
    Key? key,
    required this.text,
  }) : super(key: key);

  static const String route = '/hello';

  final String text;

  @override
  _HelloPageState createState() => _HelloPageState();
}

class _HelloPageState extends State<HelloPage> {
  String _returnValue = '';

  @override
  Widget build(BuildContext context) {
    final DateTime franzWAlter = DateTime.now();
    franzWAlter
      ..add(Duration(hours: 3))
      ..add(Duration(hours: 3))
      ..subtract(Duration(hours: 3));
    var padding2 = Padding(
      padding: const EdgeInsets.all(8),
      child: JointText(widget: widget),
    );
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Icon(Icons.face),
            Text('Hello WOrld!'),
          ],
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Card(
              color: Colors.blueGrey,
              child: _buildPadding(),
            ),
            Card(
              color: Colors.blueGrey,
              child: padding2,
            ),
            ElevatedButton(
              onPressed: () async {
                final text =
                    ModalRoute.of(context)!.settings.arguments! as String;
                final test = await showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return Dialog(
                      backgroundColor: Colors.transparent,
                      child: Card(
                        child: Container(
                          height: MediaQuery.of(context).size.height / 2,
                          width: MediaQuery.of(context).size.width / 2,
                          child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  TextField(
                                    onChanged: (String value) {
                                      if (value.length >= 5) {
                                        Navigator.of(context).pop(value);
                                      }
                                    },
                                  ),
                                  Text(text),
                                ],
                              )),
                        ),
                      ),
                    );
                  },
                ).then((value) => _returnValue = value);
                Navigator.of(context).pop(_returnValue);
              },
              child: Text('Push it!!'),
            ),
          ],
        ),
      ),
    );
  }

  Padding _buildPadding() {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Text(widget.text),
    );
  }
}

class JointText extends StatelessWidget {
  const JointText({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final HelloPage widget;

  @override
  Widget build(BuildContext context) {
    return Text('${widget.text} ♥♥');
  }
}
