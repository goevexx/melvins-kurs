import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_pages_and_forms/models/recipes.dart';

class RecipesPage extends StatelessWidget {
  RecipesPage({Key? key}) : super(key: key);
  static const String route = '/recipes';

  final List<Recipe> recipes = [
    Recipe(
        name: 'Kartoffelpürre',
        zubereitung: 'Kartoffeln Stampfen und rein die Scheiße!',
        zutaten: [
          '13 Kartoffel',
          '400g Butter',
          '100g Cannabis♥',
        ]),
    Recipe(
        name: 'Lasagne',
        zubereitung:
            'Hackfleisch kochen, Lasagneblätter auslegen und Hackfleisch drüber.',
        zutaten: [
          'Hackfleisch',
          'Tomatensauce',
          'Lasagneblätter',
          'Käse',
        ]),
    Recipe(
        name: 'Spaghetti Cabonara',
        zubereitung:
            'Eier Sahne und Parmesan in eine Schüssel geben. Spaghetti kochen. Speck anbraten. Soße mit Gewürzen zusammenfügen.',
        zutaten: [
          'Parmesan',
          'Sahne',
          'Eier',
          'Spaghetti',
          'Speck',
          'ja',
          ' Salz',
          'Pfeffer',
          'Muskat',
        ]),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rezepte'),
      ),
      body: SafeArea(
        child: ListView.builder(
          itemCount: recipes.length,
          itemBuilder: (BuildContext context, int index) {
            final recipe = recipes[index];
            return Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${recipe.name}',
                    style: GoogleFonts.habibi(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text('${recipe.zubereitung}'),
                  ...recipe.zutaten.map((zutat) => Text(zutat)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      ElevatedButton(onPressed: null, child: Text('B1')),
                      ElevatedButton(onPressed: null, child: Text('B2')),
                      ElevatedButton(onPressed: null, child: Text('B3')),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  void test(List<String> s) {
    print(s);
  }
}
