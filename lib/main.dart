import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test_pages_and_forms/models/joke_request.dart';
import 'package:test_pages_and_forms/pages/hello_page.dart';
import 'package:test_pages_and_forms/pages/recipes_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
      onGenerateRoute: (RouteSettings settings) {
        if (settings.name == HelloPage.route) {
          return MaterialPageRoute(
            settings: settings,
            builder: (BuildContext context) =>
                HelloPage(text: settings.arguments as String),
          );
        }
        if (settings.name == RecipesPage.route) {
          return MaterialPageRoute(
            builder: (BuildContext context) => RecipesPage(),
          );
        }
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _form = GlobalKey();

  String _passText = '';
  String _showValue = '';
  late Future<String> _joke;

  @override
  void initState() {
    super.initState();
    _setJoke();
  }

  Future<String> _setJoke() => _joke =
      Future.delayed(Duration(seconds: 1)).then((delay) => getRandomJoke());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () =>
                  Navigator.of(context).pushNamed(RecipesPage.route),
              child: Text('Rezepte'),
            ),
            Text(_showValue),
            ElevatedButton(
              onPressed: () async {
                if (_form.currentState!.validate()) {
                  _form.currentState!.save();
                  final _showValue = await Navigator.of(context)
                          .pushNamed(HelloPage.route, arguments: _passText)
                      as String;
                  setState(() => this._showValue = _showValue);
                }
              },
              child: Text('Hallo Page'),
            ),
            Form(
              key: _form,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      onSaved: save,
                      validator: (value) {
                        final source =
                            r"""^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+""";
                        if (RegExp(source).hasMatch(value ?? '')) {
                          return null;
                        }
                        return 'Ungültige E-Mail Adresse';
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: TextFormField(
                      decoration: InputDecoration(
                        icon: IconButton(
                          onPressed: () => showDialog(
                            context: context,
                            builder: (context) => Dialog(
                              child: Center(
                                child: Text('HI'),
                              ),
                            ),
                          ),
                          icon: Icon(Icons.add_alarm),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder<Object>(
                future: _setJoke(),
                builder: (context, snapshot) {
                  return GestureDetector(
                    onTap: () async => setState(() {
                      _setJoke();
                    }),
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: snapshot.connectionState == ConnectionState.done
                            ? Text(snapshot.data as String)
                            : CircularProgressIndicator(),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  Future<String> getRandomJoke() async {
    final uri = Uri.parse('http://api.icndb.com/jokes/random');
    final json = await http.get(uri);
    JokeRequest jr = JokeRequest.fromJson(jsonDecode(json.body));
    return jr.value.joke;
  }

  void save(value) {
    _passText = value ?? '';
  }
}
